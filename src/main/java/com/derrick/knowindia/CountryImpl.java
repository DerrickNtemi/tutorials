/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derrick.knowindia;


import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(serviceName = "DiscoverIndia")
public class CountryImpl implements Country {
      private final Utility states;
    public CountryImpl(){
        states=new Utility();
        states.loadData();
    }
    @WebMethod
    @Override
    public String getCapital(String stateName) {
        return states.getState(stateName).getCapital();
    }
    @WebMethod
    @Override
    public String getLanguages(String stateName) {
        return states.getState(stateName).getLanguages();
    }
    @WebMethod
    @Override
    public String getAirports(String stateName) {
        return states.getState(stateName).getAirports();
    }
    @WebMethod
    @Override
    public int getDistricts(String stateName) {
        return states.getState(stateName).getDistricts();
    }
    @WebMethod
    @Override
    public String getPlacesToVisit(String stateName) {
        return states.getState(stateName).getPlacesToVisit();
    }
    @WebMethod
    @Override
    public String getInterestingFacts(String stateName) {
        return states.getState(stateName).getInterestingFacts();
    }
}

